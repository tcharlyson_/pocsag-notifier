#!/bin/sh

# Install dependencies
sudo apt-get update
sudo apt-get -y install git cmake build-essential libusb-1.0 qt4-qmake qt5-default libpulse-dev libx11-dev

# Fetch and compile rtl-sdr source
mkdir -p ~/src/
cd ~/src/
git clone git://git.osmocom.org/rtl-sdr.git
cd rtl-sdr
mkdir build
cd build
cmake ../ -DINSTALL_UDEV_RULES=ON
make
sudo make install
sudo ldconfig

# Fetch and compile multimonNG
cd ~/src/
git clone https://github.com/EliasOenal/multimonNG.git
cd multimonNG
mkdir build
cd build
qmake ../multimon-ng.pro
make
sudo make install

# Fetch and compile node
if [ -n $1  ] && [ $1 = 'old' ]
then
  cd /home/pi/Documents/
  wget https://nodejs.org/dist/v11.11.0/node-v11.11.0-linux-armv6l.tar.xz
  tar -xzf node-v11.11.0-linux-armv6l.tar.gz
  cd node-v11.11.0-linux-armv6l/
  sudo cp -R * /usr/local/
  cd ..
  rm -rf node-v11.11.0-linux-armv6l
  rm -rf node-v11.11.0-linux-armv6l.tar.gz
else
  cd /home/pi/Documents/
  wget https://nodejs.org/dist/v11.11.0/node-v11.11.0-linux-armv7l.tar.xz
  tar -xzf node-v11.11.0-linux-armv7l.tar.gz
  cd node-v11.11.0-linux-armv7l/
  sudo cp -R * /usr/local/
  cd ..
  rm -rf node-v11.11.0-linux-armv7l
  rm -rf node-v11.11.0-linux-armv7l.tar.gz
fi


