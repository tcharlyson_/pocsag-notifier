const moment = require('moment');
const readline = require('readline');
const runner = require('./runner');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.on('line', (line) => {
  console.log('kek',line)
  let datetime = moment().unix();
  let address;
  let message;
  let form = false;
  let trimMessage;
  let sendFunctionCode = false;
  
  if (/^POCSAG(\d+): Address: /.test(line)) {
    address = line.match(/POCSAG(\d+): Address:(.*?)Function/)[2].trim();
    if (sendFunctionCode) {
      address += line.match(/POCSAG(\d+): Address:(.*?)Function: (\d)/)[3];
    }
    if (line.indexOf('Alpha:') > -1) {
      message = line.match(/Alpha:(.*?)$/)[1].trim();
      trimMessage = message.replace(/<[A-Za-z]{3}>/g, '').replace(/Ä/g, '[').replace(/Ü/g, ']');
    } else if (line.indexOf('Numeric:') > -1) {
      message = line.match(/Numeric:(.*?)$/)[1].trim();
      trimMessage = message.replace(/<[A-Za-z]{3}>/g, '').replace(/Ä/g, '[').replace(/Ü/g, ']');
    } else {
      message = false;
      trimMessage = '';
    }
  } else if (line.indexOf('FLEX: ') > -1) {
    address = line.match(/FLEX:.*?\[(\d*?)\] /)[1].trim();
    if (line.match(/( ALN | GPN | NUM)/)) {
      if (line.match(/ [0-9]{4}\/[0-9]\/F\/. /)) {
        // message is fragmented, hold onto it for next line
        frag[address] = line.match(/FLEX:.*?\[\d*\] ... (.*?)$/)[1].trim();
        message = false;
        trimMessage = '';
      } else if (line.match(/ [0-9]{4}\/[0-9]\/C\/. /)) {
        // message is a completion of the last fragmented message
        message = line.match(/FLEX:.*?\[\d*\] ... (.*?)$/)[1].trim();
        trimMessage = frag[address] + message;
        delete frag[address];
      } else if (line.match(/ [0-9]{4}\/[0-9]\/K\/. /)) {
        // message is a full message
        message = line.match(/FLEX:.*?\[\d*\] ... (.*?)$/)[1].trim();
        trimMessage = message;
      } else {
        // message doesn't have the KFC flags, treat as full message
        message = line.match(/FLEX:.*?\[\d*\] ... (.*?)$/)[1].trim();
        trimMessage = message;
      }
    }
  } else {
    address = '';
    message = false;
    trimMessage = '';
  }

  if (address.length > 2 && message) {
    form = {
      address: address,
      message: trimMessage,
      datetime: datetime,
    };
    runner.getCredentials(form)
  }
})