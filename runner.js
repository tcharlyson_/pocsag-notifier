#!/usr/bin/env node

const moment = require('moment-timezone');
const { spawn } = require('child_process');
const config = require('./config.json');
const axios = require('axios')
const reader = require('./reader')
const Push = require('pushover-notifications');
const winston = require('winston');
const { combine, timestamp } = winston.format;

const instance = axios.create({
  baseURL: config.url,
});

const logger = winston.createLogger({
  level: 'info',
  format: combine(
    timestamp(),
    winston.format.json(),
  ),
  defaultMeta: { service: 'user-service' },
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log` 
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'combined.log' })
  ]
});

async function launch() {
  logger.log({
    level: 'info',
    message: 'Startup successful'
  });

  // GET CASERNE AND FREQUENCY FROM TOKEN

  try {
    let caserne = await instance.get('/casernes/auth?token=' + config.token)
    if (caserne.data == null) return logger.log({
      level: 'error',
      message: 'No caserne found with this token'
    });

    logger.log({
      level: 'info',
      message: 'Caserne retrieved successfully',
      meta: caserne.data
    });
    run(caserne.data.frequency)
  } catch (error) {
    logger.log({
      level: 'error',
      message: error
    });
  }
}

module.exports = {
  // RUN LISTENER POCSAG AND WAIT FOR DATA
  run: (frequency) => {
    let child = spawn('rtl_fm -s 22050 -f ' + frequency + 'M | multimon-ng -t raw -a POCSAG512 -a POCSAG1200 -a POCSAG2400 -f alpha /dev/stdin')
    // const query = "POCSAG512: Address: 335612  Function: 0  Alpha: `date` Test message, disregard<EOT>"
    // let child = spawn('echo', query.split(' '))
    child.stdout.on('data', async (data) => {
      // console.log('data',data)
      const pocsag = reader.read(data.toString());
      // console.log('data', data)
      if (pocsag && pocsag.address) {
        getCredentials(pocsag)
      }
    });

    child.on('error', (data) => {
      logger.log({
        level: 'error',
        message: data.toString()
      });
    });

    child.on('exit', (code) => {
      logger.log({
        level: 'error',
        message: 'Application exited with code' + code.toString()
      });
    });
  }, getAlert: async (alert) => {
    return instance.get('/alerts/last?message=' + alert.message + '&configurationId=' + alert.configurationId);
  }, updateAlert: async (alert) => {
    return instance.put('/alerts/' + alert.id + '?token=' + config.token, {
      acknowledged: alert.data.acknowledged,
      acknowledged_at: moment.unix(alert.data.acknowledged_at).tz('Europe/Paris').format(),
    })
  },getCredentials: async (pocsag) => {
    try {
      // GET USER BIP
      let configuration = await instance.get('/configurations/credentials?capcode=' + pocsag.address)
      if (!configuration || configuration.data == null) return logger.log({
        level: 'error',
        message: 'No configuration for the provided capcode'
      });

      configuration = configuration.data

      logger.log({
        level: 'info',
        message: 'Configuration retrieved successfully',
        meta: configuration
      });
      
      const lastAlert = await module.exports.getAlert({ message: pocsag.message, configurationId: configuration.id })
      if (lastAlert && lastAlert.data && lastAlert.data[0] && (lastAlert.data[0].message == pocsag.message) && moment().diff(moment(lastAlert.data[0].createdAt), 'minutes') <= 2) return logger.log({
        level: 'error',
        message: 'One alert already sent less than 2 minutes ago'
      });
      
      // SEND NOTIFICATION
      if (configuration.pushover_key != null 
        && configuration.pushover_token != null 
        && configuration.pushover_token != ''
        && configuration.pushover_key != '') {

        const pocsagData = {
          user: configuration.pushover_key,
          token: configuration.pushover_token,
          message: pocsag.message
        }

        const alertData = await module.exports.sendAlert({ message: pocsag.message, time: moment().format(), configurationId: configuration.id })
        logger.log({
          level: 'info',
          message: 'Alert has been sent successfully',
          meta: alertData.data
        });
        const notificationData = await module.exports.sendNotification(pocsagData)
        const callbackData = await module.exports.verifyAknowledge({ receipt: notificationData.receipt, token: configuration.pushover_token })
        if (callbackData && callbackData.acknowledged) {
          const alertUpdatedData = module.exports.updateAlert({ id: alertData.data.id, data: callbackData })
          logger.log({
            level: 'info',
            message: 'Alert has been updated successfully',
            meta: alertUpdatedData.data
          });
        }
      } else {
        const alertData = await module.exports.sendAlert({ message: pocsag.message, time: null, configurationId: configuration.id })
        logger.log({
          level: 'info',
          message: 'Alert has been sent successfully',
          meta: alertData.data
        });
      }
    } catch (error) {
      logger.log({
        level: 'error',
        message: error
      });
    }  
  },sendNotification: async (pocsag) => {
    let pushOver = new Push({ user: pocsag.user, token: pocsag.token })

    let msg = {
      message: pocsag.message,
      title: 'Pompiers',
      sound: 'persitent',
      priority: 2,
      retry: 30,
      expire: 1800
    }

    return new Promise((resolve, reject) => {
      pushOver.send(msg, (err, result) => {
        if (err) {
          logger.log({
            level: 'error',
            message: err
          });
          reject(err)
        }
        logger.log({
          level: 'info',
          message: 'Notification has been sent successfully',
          meta: JSON.parse(result)
        });
        resolve(JSON.parse(result))
      })
    })
  },sendAlert: async (alert) => {
    return instance.post('/alerts?token=' + config.token, { 
      message: alert.message,
      notification_sent: alert.time,
      configurationId: alert.configurationId 
    })
  },verifyAknowledge: async (notification) => {
    return new Promise((resolve, reject) => {
      let turns = 0
      let interval = setInterval(async () => {
        turns += 1
        let result = await axios.get('https://api.pushover.net/1/receipts/' + notification.receipt + '.json?token=' + notification.token)

        if (turns > 60 || (result && result.data && result.data.acknowledged)) {
          clearInterval(interval)
          if (result && result.data != null) {
            logger.log({
              level: 'info',
              message: 'Alert has been acknowledged',
              meta: result.data
            });
            resolve(result.data)
          } else {
            logger.log({
              level: 'error',
              message: 'Nobody acknowledged the alert'
            });
            reject('No data')
          }
        }
      }, 10000)
    })
  }
}

// launch()
// run('85.955')