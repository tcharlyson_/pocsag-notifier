#!/bin/bash

rtl_fm -s 22050 -f 85.955M | multimon-ng -t raw -a POCSAG512 -a POCSAG1200 -a POCSAG2400 -f alpha /dev/stdin | node /home/pi/Documents/pocsag-notifier/reader.js
